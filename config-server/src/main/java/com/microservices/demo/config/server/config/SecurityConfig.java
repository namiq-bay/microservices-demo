package com.microservices.demo.config.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity security) throws Exception {
        security.ignoring()
                .antMatchers("/encrypt/**")
                .antMatchers("/decrypt/**")
                .antMatchers("/actuator/health");
        super.configure(security);
    }
}
