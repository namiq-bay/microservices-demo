#!/bin/sh

check_server_status() {
  local server_name="$1"
  local server_endpoint="$2"
  local max_retries="10"
  local retry_interval="3"
  local health_check_path="/actuator/health"
  local server_url="$server_endpoint$health_check_path"

  for ((i=1; i<=$max_retries; i++)); do
    response=$(curl -s "$server_url")
    if jq -e '.status' <<< "$response" > /dev/null; then
      status=$(jq -r '.status' <<< "$response")
      if [ "$status" = "UP" ]; then
        return 0  # Success
      else
        echo "${server_name} is not UP yet. Retrying in $retry_interval seconds..."
      fi
    else
      echo "Invalid JSON response."
    fi
    sleep "$retry_interval"
  done

  echo "Max retries reached. Unable to determine ${server_name} status."
  return 1  # Failure
}

start_server() {
  local server_name="$1"

  echo "Starting ${server_name}.."
  docker-compose -f common.yml -f "$server_name.yml" up -d
}

check_and_start_server() {
  local server_name="$1"
  local server_endpoint="$2"

  start_server "$server_name"
  check_server_status "$server_name" "$server_endpoint"
  status_code="$?"

  if [ "$status_code" -eq 0 ]; then
    echo "${server_name} is UP. Continue with the next steps."
  else
    echo "Failed to determine ${server_name} status. Exiting."
    exit 1
  fi
}

# Config Server
server_name="config-server"
server_endpoint="http://localhost:8888"

check_and_start_server "$server_name" "$server_endpoint"

# TwitterToKafkaService
server_name="twitter-to-kafka-service"
server_endpoint="http://localhost:5005"

check_and_start_server "$server_name" "$server_endpoint"

# KafkaToElasticService
server_name="kafka-to-elastic-service"
server_endpoint="http://localhost:5006"

check_and_start_server "$server_name" "$server_endpoint"

# ElasticQueryService
server_name="elastic-query-service"
server_endpoint="http://localhost:5008"

check_and_start_server "$server_name" "$server_endpoint"