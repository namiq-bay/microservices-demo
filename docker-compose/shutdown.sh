echo "Config Server shutting down.."
docker-compose -f common.yml -f config-server.yml down
echo "TwitterToKafka Service shutting down.."
docker-compose -f common.yml -f twitter-to-kafka-service.yml down
echo "KafkaToElastic Service shutting down.."
docker-compose -f common.yml -f kafka-to-elastic-service.yml down
#echo "Kafka cluster shutting down.."
#docker-compose -f common.yml -f kafka_cluster.yml down
echo "Shutting down completed!"